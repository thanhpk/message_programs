package firebaseconnect

import (
	"context"
	"log"
	"strconv"
	"time"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

var ctx = context.Background()

func ConnectFireBase() *firestore.Client {
	opt := option.WithCredentialsFile("./firebaseconnect/message-88fa2-firebase-adminsdk-mkx5q-5ae248a008.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln(err)
	}
	return client
}

func InsertData(client *firestore.Client, receiver string, message string) {
	time, _ := strconv.Atoi(time.Now().Format("20060102150405"))
	_, _, err := client.Collection("messages").Add(ctx, map[string]interface{}{
		"receiver": receiver,
		"message":  message,
		"time":     time,
	})

	if err != nil {
		log.Fatalf("Failed adding alovelace: %v", err)
	}
}

func ReadData(client *firestore.Client, receiver string) (string, error) {
	iter := client.Collection("messages").Where("receiver", "==", receiver).OrderBy("time", firestore.Desc).Limit(1).Documents(ctx)
	val := []map[string]interface{}{}
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			// log.Println(err)
			break
		}
		if err != nil {
			return "", err
		}
		val = append(val, doc.Data())
	}
	message, _ := val[0]["message"].(string)

	return message, nil
}
