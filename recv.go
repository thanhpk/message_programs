package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"os"

	"./firebaseconnect"
	cli "gopkg.in/urfave/cli.v1"
)

func main() {
	var receiver string
	var password string

	app := cli.NewApp()
	app.Name = "Send"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "u",
			Destination: &receiver,
			Usage:       "receiver",
		},
		cli.StringFlag{
			Name:        "p",
			Usage:       "password",
			Destination: &password,
		},
	}

	app.Action = func(c *cli.Context) error {
		if receiver == "" || password == "" {
			fmt.Println("null params")
			return nil
		}
		passwordByte := []byte(password)
		passwordMd5 := md5.Sum(passwordByte)
		key := passwordMd5[0:16]

		client := firebaseconnect.ConnectFireBase()
		messageEncryt, _ := firebaseconnect.ReadData(client, receiver)
		if messageEncryt == "" {
			fmt.Println("no message")
			return nil
		}
		message, err := decrypt(key, messageEncryt)
		if err != nil {
			log.Println("error")
			panic(err)
		}
		fmt.Println(message)
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func decrypt(key []byte, securemess string) (decodedmess string, err error) {
	cipherText, err := base64.URLEncoding.DecodeString(securemess)
	if err != nil {
		return
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	if len(cipherText) < aes.BlockSize {
		err = errors.New("Ciphertext block size is too short!")
		return
	}

	//IV needs to be unique, but doesn't have to be secure.
	//It's common to put it at the beginning of the ciphertext.
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(cipherText, cipherText)

	decodedmess = string(cipherText)
	return
}
