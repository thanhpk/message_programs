package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"os"

	"./firebaseconnect"

	cli "gopkg.in/urfave/cli.v1"
)

func main() {
	var receiver string
	var message string
	var password string

	app := cli.NewApp()
	app.Name = "Send"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "u",
			Destination: &receiver,
			Usage:       "receiver",
		},
		cli.StringFlag{
			Name:        "m",
			Usage:       "message",
			Destination: &message,
		},
		cli.StringFlag{
			Name:        "p",
			Usage:       "password",
			Destination: &password,
		},
	}

	app.Action = func(c *cli.Context) error {
		// fmt.Println(receiver, message, password)
		if receiver == "" || message == "" || password == "" {
			fmt.Println("null params")
			return nil
		}
		passwordByte := []byte(password)
		passwordMd5 := md5.Sum(passwordByte)
		key := passwordMd5[0:16]
		messageEncrypt, err := encrypt(key, message)
		if err != nil {
			log.Println("error")
			panic(err)
		}

		client := firebaseconnect.ConnectFireBase()
		firebaseconnect.InsertData(client, receiver, messageEncrypt)
		fmt.Println(messageEncrypt)
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func encrypt(key []byte, message string) (encmess string, err error) {
	plainText := []byte(message)

	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	//IV needs to be unique, but doesn't have to be secure.
	//It's common to put it at the beginning of the ciphertext.
	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], plainText)

	//returns to base64 encoded string
	encmess = base64.URLEncoding.EncodeToString(cipherText)
	return
}
